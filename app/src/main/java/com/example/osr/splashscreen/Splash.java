package com.example.osr.splashscreen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;



public class Splash extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

       final SharedPrefClass sharePref=new SharedPrefClass(Splash.this);

         Thread thread=new Thread(){

             @Override
             public void run() {

                 try {
                     sleep(1500);

                     if(sharePref.isSession()) {

                         Intent intent = new Intent(getApplicationContext(),WelcomePage.class);
                         startActivity(intent);
                         finish();
                     }
                     else
                     {
                         Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                         startActivity(intent);
                         finish();
                     }


                 } catch (InterruptedException e) {
                     e.printStackTrace();
                 }


             }
    };thread.start();
    }

}






