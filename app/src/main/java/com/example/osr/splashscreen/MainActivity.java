package com.example.osr.splashscreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity{

    EditText editTextFonnoLogin;
    EditText editTextPasswordLogin;
    Button buttonLogin;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        editTextFonnoLogin=(EditText)findViewById(R.id.editText_Fonno_Login);
        editTextPasswordLogin=(EditText)findViewById(R.id.editText_Password_Login);
        buttonLogin=(Button)findViewById(R.id.button_Login);


        buttonLogin.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View view)
            {

                SharedPrefClass sharedPref=new SharedPrefClass(MainActivity.this);


                String mFonnoByUser =  editTextFonnoLogin.getText().toString();
                String mPassByUser = editTextPasswordLogin.getText().toString();
               String mFonnoBySharedPref=sharedPref.getFon();
               String mPassBySharedPref=sharedPref.getPass();



                if (mFonnoByUser.equals(mFonnoBySharedPref) && mPassByUser.equals(mPassBySharedPref))

                {
                    sharedPref.setSession(true);

                    Intent intent=new Intent(MainActivity.this,WelcomePage.class);
                    startActivity(intent);
                    finish();
                }
                else
                {

                    Toast.makeText(getApplicationContext(),"INVALID LOGIN" ,Toast.LENGTH_LONG).show();


                }
            }
        });






        TextView textView=(TextView) findViewById(R.id.textView_register);
        String string="Don't have an account? Click here";


        SpannableString spannableString=new SpannableString(string);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {

                registerMyUser();

            }
        },23,33,0);

        textView.setMovementMethod(LinkMovementMethod.getInstance());

        textView.setText(spannableString);



    }

    public void registerMyUser() {



        Intent intent=new Intent(MainActivity.this,Register.class);
        startActivity(intent);
        finish();
    }
}
