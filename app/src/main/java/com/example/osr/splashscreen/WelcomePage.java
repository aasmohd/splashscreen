package com.example.osr.splashscreen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class WelcomePage extends AppCompatActivity {
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcomepage);





        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setter(toolbar);



    }


    public void logOut(View view)
    {
        SharedPrefClass sharedPrefClass=new SharedPrefClass(WelcomePage.this);
        sharedPrefClass.setSession(false);
        Intent intent=new Intent(WelcomePage.this,MainActivity.class);
        startActivity(intent);
        finish();

    }

    public void setter(Toolbar toolbar)
    {
        SharedPrefClass sharepref=new SharedPrefClass(WelcomePage.this);


         toolbar.setTitle("Welcome "+sharepref.getName());

        // setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public void Delete(View view) {

        new SharedPrefClass(WelcomePage.this).removeUser();

        Intent intent=new Intent(WelcomePage.this,MainActivity.class);
        startActivity(intent);
        finish();



    }
}
