package com.example.osr.splashscreen;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Register extends Activity {

    EditText editTextName;
    EditText editTextFonno,editTextPassword;
    Button buttonRegister;
    String mName,mPass;
    String mFonno;
    TextView tv;
    int year_x,month_x,day_x;
    static final int DIALOG_ID=0;
    int count4=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final Calendar calendar=Calendar.getInstance();
        day_x=calendar.get(Calendar.DAY_OF_MONTH);
        month_x=calendar.get(Calendar.MONTH);
        year_x=calendar.get(Calendar.YEAR);


        editTextName = (EditText) findViewById(R.id.editText_Name);
        editTextFonno = (EditText) findViewById(R.id.editText_Fonno);
        editTextPassword = (EditText) findViewById(R.id.editText_Password);
        buttonRegister = (Button) findViewById(R.id.button);

       // String et=editTextName.getText().toString();






    }



    public void showDialgOnButtonClick(View view)
    {

        tv=(TextView)findViewById(R.id.textView_DOB);
        showDialog(DIALOG_ID);


    }


    protected Dialog onCreateDialog(int id)
    {
        if(id==DIALOG_ID)
        {
            return new DatePickerDialog(this,dpickerlistener,year_x,month_x,day_x);
        }
        else return  null;


    }

    private DatePickerDialog.OnDateSetListener dpickerlistener=new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

            year_x=year;
            month_x=month+1;
            day_x=dayOfMonth;

           // Toast.makeText(getApplicationContext(),,Toast.LENGTH_LONG).show();
             //



            Calendar userAge = new GregorianCalendar(year_x,month_x,day_x);
            Calendar minAdultAge = new GregorianCalendar();
            minAdultAge.add(Calendar.YEAR, -18);
            if (minAdultAge.before(userAge)) {
                Toast.makeText(getApplicationContext(),"Sorry!! you are not 18+",Toast.LENGTH_LONG).show();
            }
            else
            {
                count4++;
                tv.setText(day_x +"/"+month_x+"/"+year_x);
            }
        }
    };



         public void saveData(View view) {

             int count=0;
             int count2=0;
             int count3=0;
             SharedPrefClass user=new SharedPrefClass(Register.this);
             String fonnosp=user.getFon();
             mName = editTextName.getText().toString();
             mFonno = editTextFonno.getText().toString();
             mPass = editTextPassword.getText().toString();


             if(mName.length()>2)
             {
                 user.setName(mName); count3++;


                 if(mFonno.length()==10)
                 {
                     user.setFon(mFonno); count2++;

                     if(!(mFonno.equals(fonnosp)))

                          {

                        if(mPass!=null && mPass.length()>4 && mPass.length()<9 && mPass.contains("@"))
                        {

                         user.setPass(mPass);
                         count++;


                           if (count4 != 1 )
                           {
                             Toast.makeText(getApplicationContext(), "selct date", Toast.LENGTH_LONG).show();
                           }

                        }
                     else
                     {

                         Toast.makeText(getApplicationContext(), "invalid Password", Toast.LENGTH_LONG).show();
                     }


                            } else{ Toast.makeText(getApplicationContext(),user.getFon()+" already exist",Toast.LENGTH_LONG).show();}


                 }

                 else
                 {

                     Toast.makeText(getApplicationContext(), "invalid fonno", Toast.LENGTH_LONG).show();
                 }


             }

             else
             {

                 Toast.makeText(getApplicationContext(), "invalid Username", Toast.LENGTH_LONG).show();
             }














                 if (count == 1 && count2 == 1 && count3 == 1 && count4 == 1) {
                     Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                     startActivity(intent);
                     finish();
                 }












         }




    }

