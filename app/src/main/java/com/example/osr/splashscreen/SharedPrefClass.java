package com.example.osr.splashscreen;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by osr on 21/11/17.
 */

public class SharedPrefClass {

    Context context;
    SharedPreferences sharedPreferences;

    public boolean isSession() {

        return  sharedPreferences.getBoolean("session",false);

    }

    public void setSession(boolean session) {
        this.session = session;
        sharedPreferences.edit().putBoolean("session",session).commit();

    }

    private boolean session;



    public void removeUser()
    {
        sharedPreferences.edit().clear().commit();

    }

    public String getName() {
        name= sharedPreferences.getString("userdata","");
        return name;
    }

    public void setName(String name) {
        this.name = name;
        sharedPreferences.edit().putString("userdata",name).commit();
    }

    private String name;

    public String getFon() {

        Fon= sharedPreferences.getString("userdata2","5");


        return Fon;
    }

    public void setFon(String fon) {

        this.Fon = fon;
        sharedPreferences.edit().putString("userdata2",fon).commit();

    }

    private String Fon;

    public String getPass() {

        Pass= sharedPreferences.getString("userdata3","9");

        return Pass;
    }

    public void setPass(String pass) {
        this.Pass = pass;
        sharedPreferences.edit().putString("userdata3",pass).commit();

    }

    private String Pass;


    public SharedPrefClass(Context context)
    {
        this.context=context;
        sharedPreferences=context.getSharedPreferences("userinfo",Context.MODE_PRIVATE);




    }



}
